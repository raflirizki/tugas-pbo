<?php
// Bangun Datar  
abstract class BangunDatar {
    public string $name;
    public float $panjang;
    public float $lebar;

    abstract public function keliling():float;

    abstract public function luas();

    public function name():string {
        return $this->name;
    }
}

// 1. Persegi
class Persegi extends BangunDatar {
    public float $sisi;

    public function __construct()
    {
        $this->name="Persegi";
        $this->sisi=5;
    }

    public function keliling(): float
    {
        $keliling = 4 * $this->sisi;
        return $keliling;
    }

    public function luas()
    {
        $luas = pow($this->sisi, 2);
        return $luas;
    }
}

// 2. Persegi Panjang
class PersegiPanjang extends BangunDatar {
    public function __construct()
    {
        $this->name="Persegi Panjang";
        $this->panjang=4;
        $this->lebar=5;
    }
    
    public function keliling(): float
    {
        $keliling = 2 * ($this->panjang + $this->lebar);
        return $keliling;
    }

    public function luas()
    {
        $luas = $this->panjang * $this->lebar;
        return $luas;
    }
}

// 3. Segitiga
class Segitiga extends BangunDatar {
    public float $alas;
    public float $tinggi;
    public float $a;
    public float $b;
    public float $c;

    public function __construct()
    {
        $this->name="Segitiga";
        $this->alas=4;
        $this->tinggi=5;
        $this->a=4;
        $this->b=3;
        $this->c=3;
    }

    public function keliling(): float
    {
        $keliling = $this->a + $this->b + $this->c;
        return $keliling;
    }

    public function luas()
    {
        $luas = ($this->alas * $this->tinggi) / 2;
        return $luas;
    }
}

// 4. Lingkaran
class Lingkaran extends BangunDatar {
    public float $jarijari;

    public function __construct()
    {
        $this->name="Lingkaran";
        $this->jarijari=5;
    }

    public function keliling(): float
    {
        $keliling = 2 * pi() * $this->jarijari;
        return $keliling;
    }

    public function luas()
    {
        $luas = pi() * pow($this->jarijari, 2);
        return $luas;
    }
}

// 5. Trapesium
class Trapesium extends BangunDatar {
    public float $tinggi;
    public float $a;
    public float $b;
    public float $c;
    public float $d;

    public function __construct()
    {
        $this->name="Trapesium";
        $this->tinggi=5;
        $this->a=6;
        $this->b=5;
        $this->c=4;
        $this->d=4;
    }

    public function keliling(): float
    {
        $keliling = $this->a + $this->b + $this->c + $this->d;
        return $keliling;
    }

    public function luas()
    {
        $luas = (($this->a + $this->b) / 2) * $this->tinggi;
        return $luas;
    }
}

// 6. Jajar Genjang
class JajarGenjang extends BangunDatar {
    public float $tinggi;
    public float $a;
    public float $b;
    public float $c;
    public float $d;

    public function __construct()
    {
        $this->name="Jajar Genjang";
        $this->tinggi=5;
        $this->a=5;
        $this->b=5;
        $this->c=4;
        $this->d=4;
    }

    public function keliling(): float
    {
        $keliling = $this->a + $this->b + $this->c + $this->d;
        return $keliling;
    }

    public function luas()
    {
        $luas = $this->a * $this->tinggi;
        return $luas;
    }
}

// 7. Belah Ketupat
class BelahKetupat extends BangunDatar {
    public float $diagonal_1;
    public float $diagonal_2;
    public float $sisi;
    
    public function __construct()
    {
        $this->name="Belah Ketupat";
        $this->diagonal_1=4;
        $this->diagonal_2=5;
        $this->sisi=6;
    }

    public function keliling(): float
    {
        $keliling = 4 * $this->sisi;
        return $keliling;
    }

    public function luas()
    {
        $luas = ($this->diagonal_1 * $this->diagonal_2) / 2;
        return $luas;
    }
}

// Menampilkan hasil
$bangundatar = new BelahKetupat();
echo "Nama: ";
echo $bangundatar->name()."<br>";
echo "Keliling = ";
echo $bangundatar->keliling()."<br>";
echo "Luas = ";
echo $bangundatar->luas()."<br>";
?>
