<?php  
// Makhluk Hidup
class MakhlukHidup {
    public string $name;
    public string $numberOfLegs;
    public string $respirationOrgan;
    public string $foodClassification;
    public bool $canRun;
    public bool $canSwim;
    public bool $canFly;
    public bool $canCry;

    public function name():string {
        return $this->name;
    }

    public function legs():string {
        return $this->numberOfLegs;
    }

    public function respirationOrgan():string {
        return $this->respirationOrgan;
    }

    public function eat():string {
        return $this->foodClassification;
    }

    public function run():string {
        if ($this->canRun == true) {
            return "Yes, it can run";
        } else {
            return "No, it can't run";
        }
    }

    public function swim():string {
        if ($this->canSwim == true) {
            return "Yes, it can swim";
        } else {
            return "No, it can't swim";
        }
    }

    public function fly():string {
        if ($this->canFly == true) {
            return "Yes, it can fly";
        } else {
            return "No, it can't fly";
        }
    }

    public function cry():string {
        if ($this->canCry == true) {
            return "Yes, it can cry";
        } else {
            return "No, it can't cry";
        }
    }
}

// 1. CatFish
class CatFish extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Cat Fish";
        $this->numberOfLegs=0;
        $this->respirationOrgan="Gills";
        $this->foodClassification="Omnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 2. BettaFish
class BettaFish extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Betta Fish";
        $this->numberOfLegs=0;
        $this->respirationOrgan="Gills";
        $this->foodClassification="Carnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 3. Crocodile
class Crocodile extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Crocodile";
        $this->numberOfLegs=4;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 4. Alligator
class Alligator extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Alligator";
        $this->numberOfLegs=4;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 5. Lizard
class Lizard extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Lizard";
        $this->numberOfLegs=4;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 6. Snake
class Snake extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Snake";
        $this->numberOfLegs=0;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 7. Swan
class Swan extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Swan";
        $this->numberOfLegs=2;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Omnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=true;
        $this->canCry=false;
    }
}

// 8. Chicken
class Chicken extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Chicken";
        $this->numberOfLegs=2;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Omnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 9. Swallow
class Swallow extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Swallow";
        $this->numberOfLegs=2;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Insectivore";
        $this->canRun=false;
        $this->canSwim=false;
        $this->canFly=true;
        $this->canCry=false;
    }
}

// 10. Duck
class Duck extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Duck";
        $this->numberOfLegs=2;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Omnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=true;
        $this->canCry=false;
    }
}

// 11. Human
class Human extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Human";
        $this->numberOfLegs=2;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Omnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=true;
    }
}

// 12. Whale
class Whale extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Whale";
        $this->numberOfLegs=0;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 13. Dolphin
class Dolphin extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Dolphin";
        $this->numberOfLegs=0;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// 14. Bat
class Bat extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Bat";
        $this->numberOfLegs=4;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Omnivore";
        $this->canRun=false;
        $this->canSwim=true;
        $this->canFly=true;
        $this->canCry=false;
    }
}

// 15. Tiger
class Tiger extends MakhlukHidup {
    public function __construct()
    {
        $this->name="Tiger";
        $this->numberOfLegs=4;
        $this->respirationOrgan="Lungs";
        $this->foodClassification="Carnivore";
        $this->canRun=true;
        $this->canSwim=true;
        $this->canFly=false;
        $this->canCry=false;
    }
}

// Menampilkan hasil
$makhlukhidup = new Tiger();
echo "Name: ";
echo $makhlukhidup->name()."<br>";
echo "Legs: ";
echo $makhlukhidup->legs()."<br>";
echo "Respiration Organ: ";
echo $makhlukhidup->respirationOrgan()."<br>";
echo "Eat: ";
echo $makhlukhidup->eat()."<br>";
echo "Run: ";
echo $makhlukhidup->run()."<br>";
echo "Swim: ";
echo $makhlukhidup->swim()."<br>";
echo "Fly: ";
echo $makhlukhidup->fly()."<br>";
echo "Cry: ";
echo $makhlukhidup->cry()."<br>";
?>
